<?php

/**
 * @file
 * uw_ct_feds_webpage_with_banner.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_webpage_with_banner_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_web_page_with_hero content'.
  $permissions['create feds_web_page_with_hero content'] = array(
    'name' => 'create feds_web_page_with_hero content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_web_page_with_hero content'.
  $permissions['delete any feds_web_page_with_hero content'] = array(
    'name' => 'delete any feds_web_page_with_hero content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_web_page_with_hero content'.
  $permissions['delete own feds_web_page_with_hero content'] = array(
    'name' => 'delete own feds_web_page_with_hero content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_web_page_with_hero content'.
  $permissions['edit any feds_web_page_with_hero content'] = array(
    'name' => 'edit any feds_web_page_with_hero content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_web_page_with_hero content'.
  $permissions['edit own feds_web_page_with_hero content'] = array(
    'name' => 'edit own feds_web_page_with_hero content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_web_page_with_hero revision log entry'.
  $permissions['enter feds_web_page_with_hero revision log entry'] = array(
    'name' => 'enter feds_web_page_with_hero revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_web_page_with_hero authored by option'.
  $permissions['override feds_web_page_with_hero authored by option'] = array(
    'name' => 'override feds_web_page_with_hero authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_web_page_with_hero authored on option'.
  $permissions['override feds_web_page_with_hero authored on option'] = array(
    'name' => 'override feds_web_page_with_hero authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Export perm: 'override feds_web_page_with_hero promote to front pg option'.
  $permissions['override feds_web_page_with_hero promote to front page option'] = array(
    'name' => 'override feds_web_page_with_hero promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_web_page_with_hero published option'.
  $permissions['override feds_web_page_with_hero published option'] = array(
    'name' => 'override feds_web_page_with_hero published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_web_page_with_hero revision option'.
  $permissions['override feds_web_page_with_hero revision option'] = array(
    'name' => 'override feds_web_page_with_hero revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_web_page_with_hero sticky option'.
  $permissions['override feds_web_page_with_hero sticky option'] = array(
    'name' => 'override feds_web_page_with_hero sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_web_page_with_hero content'.
  $permissions['search feds_web_page_with_hero content'] = array(
    'name' => 'search feds_web_page_with_hero content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search uw_web_page content'.
  $permissions['search uw_web_page content'] = array(
    'name' => 'search uw_web_page content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
