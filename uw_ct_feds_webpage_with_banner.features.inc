<?php

/**
 * @file
 * uw_ct_feds_webpage_with_banner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_webpage_with_banner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_webpage_with_banner_node_info() {
  $items = array(
    'feds_web_page_with_hero' => array(
      'name' => t('Feds web page with hero'),
      'base' => 'node_content',
      'description' => t('Primarily created for category landing pages with the option of including a hero image.'),
      'has_title' => '1',
      'title_label' => t('Heading'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
