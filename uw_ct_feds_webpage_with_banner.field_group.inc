<?php

/**
 * @file
 * uw_ct_feds_webpage_with_banner.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_webpage_with_banner_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload|node|feds_web_page_with_hero|form';
  $field_group->group_name = 'group_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_web_page_with_hero';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '4',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_upload|node|feds_web_page_with_hero|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Upload an image');

  return $field_groups;
}
